//CH1 console.log("HELLO WORLD");

/*CH2
var res  = 0;
var args = process.argv;

function parseIntForSum(str) {
  var possibleInteger = parseInt(str);
  return isNaN(possibleInteger) ? 0 : possibleInteger;
}

function sum(f, s) {
  return parseIntForSum(f) + parseIntForSum(s);
}

for (arg in args) {
  if (arg => 2) {
    res = sum(res, args[arg]);
  }
}

console.log(res);

/// Simple version ////
var result = 0;

for (var i = 2; i < process.argv.length; i++) {
  result += Number(process.argv[i]);
}

console.log(result);
*/

/*CH 3
var fs = require('fs');
var buffer = fs.readFileSync(process.argv[2]);

var total = buffer.toString().split('\n').length - 1;
console.log(total);
*/

/*CH 4
var fs = require('fs');
fs.readFile(process.argv[2], (err, data) => {
  if (err) throw err;
  
  var total = data.toString().split('\n').length - 1;
  console.log(total);
});
*/

/*CH 5
var fs   = require('fs'),
    path = require('path');

fs.readdir(process.argv[2], (err, list) => {
  if (err) console.log(err);

  for (var i = 0; i < list.length; i++) {
    var ext = path.extname(list[i]);

    if (ext === '.' + process.argv[3])
      console.log(list[i]);
  }
});
*/

/*CH 6
var mymodule = require('./filter.js');

mymodule(process.argv[2], process.argv[3], function(err, res) {
  if (err) console.log(err);

  for (result of res) {
    console.log(result);
  }
});
*/

/*CH 7
var http = require('http');
var url  = process.argv[2];

http.get(url, function(response) {
  response.on('data', function(data) {
    console.log(data.toString());
  });
}).on('data', function(data) {
  console.log("Your data: " + data.toString());
}).on('error', function(error) {
  console.log('An error occured: ' + error);
}).on('end', function(){
  console.log('Program ended');
});

/////////// Alt ///////
var http = require('http')

http.get(process.argv[2], function (response) {
  response.setEncoding('utf8')
  response.on('data', console.log)
  response.on('error', console.error)
}).on('error', console.error)
*/

/*CH 8
var http = require('http');

http.get(process.argv[2], function (response) {
  var content = '';

  response.setEncoding('utf8');
  response.on('data', function(data) {
    content += data;
  });

  response.on('end', function() {
    console.log(content.length);
    console.log(content);
  });
});

//////// ALT using BL ///////
var http = require('http')
var bl = require('bl')

http.get(process.argv[2], function (response) {
  response.pipe(bl(function (err, data) {
    if (err) {
      return console.error(err)
    }
    data = data.toString()
    console.log(data.length)
    console.log(data)
  }))
})
*/

/*CH 9
var http = require('http');

http.get(process.argv[2], function (response) {
  var content = '';

  response.setEncoding('utf8');
  response.on('data', function(data) {
    content += data;
  });

  response.on('end', function() {
    console.log(content);
    http.get(process.argv[3], function (response) {
      var content1 = '';

      response.setEncoding('utf8');
      response.on('data', function(data) {
        content1 += data;
      });

      response.on('end', function() {
        console.log(content1);
        http.get(process.argv[4], function (response) {
          var content2 = '';

          response.setEncoding('utf8');
          response.on('data', function(data) {
            content2 += data;
          });

          response.on('end', function() {
            console.log(content2);
          });
        });
      });
    });
  });
});

/////////// ALT ///////////
var http = require('http')
var bl = require('bl')
var results = []
var count = 0

function printResults () {
  for (var i = 0; i < 3; i++) {
    console.log(results[i])
  }
}

function httpGet (index) {
  http.get(process.argv[2 + index], function (response) {
    response.pipe(bl(function (err, data) {
      if (err) {
        return console.error(err)
      }

      results[index] = data.toString()
      count++

      if (count === 3) {
        printResults()
      }
    }))
  })
}

for (var i = 0; i < 3; i++) {
  httpGet(i)
}
*/

/* CH10
var net    = require('net');
var server = net.createServer(function(socket){
  // socket handling logic
  var mdate = new Date();
  var month = (mdate.getMonth() < 10) ? '0' + parseInt(mdate.getMonth()+1) : parseInt(mdate.getMonth()+1);
  var dates = (mdate.getDate() < 10) ? '0' + mdate.getDate() : mdate.getDate();
  var hours = (mdate.getHours() < 10) ? '0' + mdate.getHours() : mdate.getHours();
  var minute = (mdate.getMinutes() < 10) ? '0' + mdate.getMinutes() : mdate.getMinutes();

  var completeDate = mdate.getFullYear() + '-' + month + '-' + dates + ' ' + hours + ':' + minute + '\n';
  socket.write(completeDate);
  socket.end();
});

server.listen(process.argv[2]);

/////////// ALT /////////////////
var net = require('net')

function zeroFill (i) {
  return (i < 10 ? '0' : '') + i
}

function now () {
  var d = new Date()
  return d.getFullYear() + '-' +
    zeroFill(d.getMonth() + 1) + '-' +
    zeroFill(d.getDate()) + ' ' +
    zeroFill(d.getHours()) + ':' +
    zeroFill(d.getMinutes())
}

var server = net.createServer(function (socket) {
  socket.end(now() + '\n')
})

server.listen(Number(process.argv[2]))
*/

/* CH11
var http = require('http'),
    fs   = require('fs');

var server = http.createServer(function(request, response) {
  response.writeHead(200, { 'content-type': 'text/plain' });

  var src = fs.createReadStream(process.argv[3]);
  src.pipe(response);
});

server.listen(Number(process.argv[2]));
*/

/* CH12
var http = require('http'),
    map  = require('through2-map');

var server = http.createServer(function(request, response) {
  if (request.method === 'POST') {
    request.pipe(map(function (chunk) {
      return chunk.toString().toUpperCase();
    })).pipe(response);
  }
});

server.listen(Number(process.argv[2]));
*/

/* CH 13
var http = require('http'),
    url  = require('url');

console.log('Server is running on port ' + Number(process.argv[2]));
var server = http.createServer(function(request, response) {
  if (request.method === 'GET') {
    var q   = url.parse(request.url, true);
    var res = null;

    switch(q.pathname) {
      case '/api/parsetime' : var dt  = new Date(q.query.iso);
                                  res = {
                                    'hour'   : dt.getHours(), 
                                    'minute' : dt.getMinutes(), 
                                    'second' : dt.getSeconds()
                                  };
                              break;
      case '/api/unixtime' : res = {'unixtime' : Date.parse(q.query.iso)};
                             break;
      default : response.writeHead(404);
                response.end();
                break; 
    }

    response.writeHead(200, { 'Content-Type': 'application/json' });
    response.write(JSON.stringify(res));
    response.end();
  }
});

server.listen(Number(process.argv[2]));

///// ALT //////////////
var http = require('http')
var url = require('url')

function parsetime (time) {
  return {
    hour: time.getHours(),
    minute: time.getMinutes(),
    second: time.getSeconds()
  }
}

function unixtime (time) {
  return { unixtime: time.getTime() }
}

var server = http.createServer(function (req, res) {
  var parsedUrl = url.parse(req.url, true)
  var time = new Date(parsedUrl.query.iso)
  var result

  if (/^\/api\/parsetime/.test(req.url)) {
    result = parsetime(time)
  } else if (/^\/api\/unixtime/.test(req.url)) {
    result = unixtime(time)
  }

  if (result) {
    res.writeHead(200, { 'Content-Type': 'application/json' })
    res.end(JSON.stringify(result))
  } else {
    res.writeHead(404)
    res.end()
  }
})
server.listen(Number(process.argv[2]))
*/