var fs   = require('fs'),
    path = require('path');

module.exports = function(dirname, extension, callback) {
  fs.readdir(dirname, (err, list) => {
    if (err) return callback(err);
    var files = [];

    for (var i = 0; i < list.length; i++) {
      var ext = path.extname(list[i]);

      if (ext === '.' + extension)
        files.push(list[i]);
    }

    callback(null, files);
  });
}