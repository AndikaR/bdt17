function Config() {
  this.express = require('express');
  this.app     = this.express();

  var bodyparser = require('body-parser');
  this.app.use(bodyparser.urlencoded({extended: false}));

  //this.app.set('view engine', 'pug');
  this.app.use(require('stylus').middleware('../public'));
  this.app.set('views', '../resources/view/');
}

Config.prototype.runMiddleware = function() {
  let middleware = require('../app/Middleware.js');
  middleware(this.express);
}

Config.prototype.runRoutes = function() {
  let routes = require('../app/Routes.js');
  routes(this.app);
}

module.exports = Config;
