var Config = require('../../../config/init.js');
var util   = require('util');
var path   = require('path');

function Base() {
  Config.apply(this, arguments);
  //this.app.set('views', path.normalize('../app/User/View'));
}

util.inherits(Base, Config);

module.exports = Base;