const express  = require('express');
const exphbs   = require('express-handlebars');
const passport = require('passport');
const path     = require('path');
const Datastore = require('nedb');

var db = new Datastore();

db = {};
db.blog = new Datastore('./blog.db');
db.blog.loadDatabase();
//var Sequelize = require('sequelize');
//const sequelize = new Sequelize('mysql://user:pass@root:900909/latihan');

const cookieParser  = require('cookie-parser');
const bodyParser    = require('body-parser');
const session       = require('express-session');

//Strategy
const LocalStrategy    = require('passport-local').Strategy;
const FacebookStrategy = require('passport-facebook').Strategy;

//Key and Secret Key
const FACEBOOK_APP_ID     = '1864935943826571';
const FACEBOOK_APP_SECRET = '4bd20097b35e3bc85a1b0b178f992cef';

const app = express();

//init view
var hbConfig = {
  layoutsDir: path.join(app.settings.views, 'layouts'), 
  defaultLayout: "master"
} 

//app.engine('html', require('express3-handlebars')(hbConfig));

app.engine('handlebars', exphbs(hbConfig));
app.set('view engine', 'handlebars');
app.set('view options', { layout: null });
app.set('views', path.join(__dirname, 'views'));
//app.set('views', 'views');

//middleware
app.use(express.static(path.join(__dirname, 'public')));
app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(session({ secret: 'cookie_secret', resave: true, saveUninitialized: true }));
app.use(passport.initialize());
app.use(passport.session());

/*
//Facebook Auth
passport.use(new FacebookStrategy({
    clientID: FACEBOOK_APP_ID,
    clientSecret: FACEBOOK_APP_SECRET,
    callbackURL: "http://localhost:8081/auth/facebook/callback"
  },
  function(accessToken, refreshToken, profile, done) {
    done(null, profile);
  }
));

function isLogged(req, res, next) {
  if (req.isAuthenticated()) return next();
  
  res.redirect('/');
}

app.get('/auth/facebook', passport.authenticate('facebook'));

app.get('/account', isLogged, function(req, res){
  res.render('account', {user: req.user});
});

app.get('/logout', function(req, res){
  req.logout();
  res.redirect('/');
});

app.get('/auth/facebook/callback',
  passport.authenticate('facebook', { 
    successRedirect: '/account',
    failureRedirect: '/login' 
  }
));

passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(user, done) {
  done(null, user);
});
*/
app.get('/store', function(req, res){
  var doc = { hello: 'world'
               , n: 5
               , today: new Date()
               , nedbIsAwesome: true
               , notthere: null
               , notToBeSaved: undefined  // Will not be saved
               , fruits: [ 'apple', 'orange', 'pear' ]
               , infos: { name: 'nedb' }
            };
  
  db.blog.insert(doc, function (err, newDoc) {   // Callback is optional
    // newDoc is the newly inserted document, including its _id
    // newDoc has no key called notToBeSaved since its value was undefined
  });

  res.render('login', {title: 'Test', content: 'Click to connect with facebook', layout: false});
});


app.get('/', function(req, res){
  res.render('login', {title: 'Test', content: 'Click to connect with facebook'});
});

app.get('/register', function(req, res){
  res.render('register');
});

app.post('/register', function(req, res){
//    
});

app.listen(8082);