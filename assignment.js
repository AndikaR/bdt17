function pInt(value) {
  return parseInt(value);
}

function get_array() {
  var data = [];

  for (var i = 3; i < process.argv.length; i++) {
    data.push(pInt(process.argv[i]));
  }

  return data;
}

function second_check(operation, var1, var2) {
  switch(operation) {
    case 'add'  :
    case 'sub'  :
    case 'mul'  :
    case 'mod'  : result = calculator[operation](var1, var2); break;
    case 'pow'  : result = calculator[operation](var1); break;
    default : result = 'Please enter a valid operation'; break;
  }
}

var calculator = {
  add : function(a, b) {
    return pInt(a) + pInt(b);
  },
  sub : function(a, b) {
    return a - b; 
  },
  mul : function(a, b) {
    return a * b;
  },
  mod : function(a, b) {
    return a % b;
  },
  pow : function(a) {
    return a * a;
  },
  mean : function(input) {
    let res = this.total(input);

    return res / pInt(input.length);
  },
  mid : function(input) {
    let res = 0;
    
    if (!this.mod(input.length, 2)) {
      let mid = input.length / 2,
            a = input[mid],
            b = input[mid-1];

        res = (a + b) / 2;
    } else {
      res = input[Math.ceil(parseFloat(input.length / 2)) - 1];
    }
    return res;
  },
  total : function(input) {
    let res = 0;

    for (var value of input) {
      res += value;
    }

    return res;
  }
}

var operation = process.argv[3];
var var1      = process.argv[2];
var var2      = process.argv[4];
var result    = 0;

switch(var1) {
  case 'mean'  :
  case 'mid'   :
  case 'total' : var arr = get_array();
                 result = calculator[var1](arr);
                 break;
  default : second_check(operation, var1, var2); break;               
}

console.log(result);