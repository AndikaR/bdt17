Person = function(name, age) {
  this.name = name;
  this.age = age;
}

Andika = new Person('Andika', '23');

//console.log(Andika);

var calculator = {
  penambahan : function(a, b) {
    return a + b;
  },
  pengurangan : function(a, b) {
    return a - b;
  },
  perkalian : function(a, b) {
    return a * b;
  },
  modulus : function(a, b) {
    return a % b;
  },
  kuadrat : function(a) {
    return a * a;
  },
  nilaiRataRata : function(input) {
    let hasil = this.nilaiTotal(input);

    return hasil / input.length;
  },
  nilaiTengah : function(input) {
    let hasil = 0;

    if (!this.modulus(input.length, 2)) {
      let tengah = input.length / 2,
          a = input[tengah],
          b = input[tengah-1];

      hasil = (a + b) / 2;
    } else {
      hasil = input[Math.ceil(input.length / 2)];
    }

    return hasil;
  },
  nilaiTotal : function(input) {
    let hasil = 0;

    for (var nilai of input) {
      hasil += nilai;
    }

    return hasil;
  }
}

console.log('penambahan : ' + calculator.penambahan(1, 2));
console.log('pengurangan : ' + calculator.pengurangan(1, 2));
console.log('perkalian : ' + calculator.perkalian(1, 2));
console.log('modulus : ' + calculator.modulus(1, 2));
console.log('kuadrat : ' + calculator.kuadrat(2));
console.log('rata2 : ' + calculator.nilaiRataRata([10, 10, 10, 5, 10, 10]));
console.log('nilai tengah : ' + calculator.nilaiTengah([10, 10, 10, 5, 10, 10]));
console.log('total : ' + calculator.nilaiTotal([1, 2, 3, 4, 5, 6]));

var counter = function() {
  var count = 0;
  return function() {
    return ++count;
  }
};

c1 = counter();

console.log(c1());
c1 = counter();

console.log(c1());
c2 = counter();

console.log(c2());

var test = {};

for (var i=0; i<4; i++) {
  (function(j) {
    test[j] = function() {
      console.log(j);
    }
  })(i);
}

test[0]();
test[1]();

//javascript prototype

var arr = [];
arr[25] = ['wasd'];

console.log(arr); 