function Environment() {
  this.express    = require('express');
  this.ejs        = require('ejs');
  
  this.app = this.express();
}

Environment.prototype.init = function() {
  var bodyParser = require('body-parser');
  var path       = require('path');
  var winston    = require('winston');
  var flash      = require('connect-flash');
  
  this.app.use(bodyParser.json()); // support json encoded bodies
  this.app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
  this.app.use(this.express.static('../public'));
  
  //Logger
  this.logger = new (winston.Logger)({
    transports: [
      new (winston.transports.File)({ filename: path.join(__dirname, '../log/debug.log') })
    ]
  });
  //End of Logger
  
  //Passport
  this.passport     = require('passport');
  this.session      = require('express-session');
  var LocalStrategy = require('passport-local').Strategy;

  this.app.use(this.session({ secret: 'randomfacts', resave: false, saveUninitialized: false })); // session secret
  this.app.use(this.passport.initialize());
  this.app.use(this.passport.session());
  this.app.use(flash());

  this.app.set('view engine', 'ejs');
  this.app.set('views', path.join(__dirname, '../resources/views/'));
}

Environment.prototype.routes = function() {
  require('../app/routes.js');
}

Environment.prototype.initPassport = function() {
  require('../config/passport.js')(this.passport);
}

module.exports = new Environment();
