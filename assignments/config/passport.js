var LocalStrategy = require('passport-local').Strategy;
var Member        = require('../app/models/member.js');
var f             = require('../lib/functions.js');

module.exports = function(passport) {
  // used to serialize the user for the session
  passport.serializeUser(function(member, done) {
    done(null, member.id);
  });

  // used to deserialize the user
  passport.deserializeUser(function(id, done) {
    Member.findById(id, function(err, member) {
      done(err, member);
    });
  });

  passport.use('local-signup', new LocalStrategy({
    usernameField : 'email',
    passwordField : 'password',
    passReqToCallback : true // allows us to pass back the entire request to the callback
  },
  function(req, email, password, done) {
    f.env.logger.info('K');
    // asynchronous
    process.nextTick(function() {
      // find a user whose email is the same as the forms email
      // we are checking to see if the user trying to login already exists
      Member.findOne({ 'local.email' :  email }, function(err, member) {
        
        if (err) return done(err);

        // check to see if theres already a user with that email
        if (member) {
          return done(null, false, req.flash('signupMessage', 'Email already taken.'));
        } else {
          var member = new Member();

          // set the user's local credentials
          member.local.email    = email;
          member.local.password = member.generateHash(password);

          // save the user
          member.save(function(err) {
            if (err) throw err;

            return done(null, newUser);
          });
        }
      });    
    });
  }));
};