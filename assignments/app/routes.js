var f = require('../lib/functions.js');

function Routes() {
  group(function(router){
    router.use(function(req, res, next){
      res.locals.success_messages = req.flash('success_messages');
      res.locals.error_messages = req.flash('error_messages');
      next();
    });

    //Index
    router.get('/', f.controller('index').home);
    
    //Member auth
    router.get('/login', f.controller('auth').login);
    router.post('/login', f.controller('auth')._login);
    router.get('/signup', f.controller('auth').signup);
    router.post('/signup', f.controller('auth')._signup);
    router.get('/logout', f.controller('auth').logout);

    //Member page
    router.get('/profile', isLoggedIn, f.controller('member').profile);
  });
}

module.exports = new Routes(); 

function group() {
  var args = Array.prototype.slice.call(arguments);
  var path = null;
  var fn   = null;

  if (args.length && args.length <= 2) {
    fn   = args.pop();
    path = (args.length) ? args.pop() : '/';
    
    var router = f.env.express.Router(); 
    
    fn(router);
    f.env.app.use(path, router);

    return router;
  }
}

function isLoggedIn(req, res, next) { 
  if (req.isAuthenticated())
    return next();

  res.redirect('/login');
}