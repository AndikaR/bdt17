var f = require('../../lib/functions.js');
var bcrypt   = require('bcrypt');
var mongoose = require('mongoose');

// define the schema for our user model
var memberSchema = mongoose.Schema({
  local : {
    email    : String,
    password : String,
  }
});

memberSchema.methods.generateHash = function(password) {
  return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

memberSchema.methods.validPassword = function(password) {
  return bcrypt.compareSync(password, this.local.password);
};

module.exports = mongoose.model('member', memberSchema);

