var ex = module.exports = {};
var f  = require('../../lib/functions.js');

ex.login = function(req, res) {
  res.render('user/auth/login', { message: req.flash('loginMessage') });
}

ex._login = function(req, res) {
}

ex.signup = function(req, res) {
  res.render('user/auth/signup', { message: req.flash('signupMessage') });
}

ex._signup = function(req, res) {
  var passport = f.env.passport;
  
  passport.authenticate('local-signup', {
    successRedirect : '/profile', // redirect to the secure profile section
    failureRedirect : '/', // redirect back to the signup page if there is an error
    failureFlash : true // allow flash messages
  });
  
  //res.redirect('/');

  /*
  var db     = f.env.db;
  var bcrypt = require('bcrypt');

  var data = {
    email    : req.body.email,
    password : bcrypt.hashSync(req.body.password, 10)
  }

  db.member.insert(data, function(err, doc) {
    if (err) f.logger.info('An error occured : ' + err.toString());
  });

  res.redirect('/login', { success_message: 'You have been signed!'});
  */
}

ex.logout = function(req, res) {
  req.logout();
  res.redirect('/login');
}

