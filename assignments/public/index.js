var env = require('../config/env.js');

env.init();
env.routes();
env.initPassport();
env.app.listen(8081);