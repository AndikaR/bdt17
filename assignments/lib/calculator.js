function toInt(val) {
  return parseInt(val);
}

var Calculator = {
  add : function(a, b) {
    return toInt(a) + toInt(b);
  },
  sub : function(a, b) {
    return a - b; 
  },
  mul : function(a, b) {
    return a * b;
  },
  mod : function(a, b) {
    return a % b;
  },
  pow : function(a) {
    return a * a;
  },
  mean : function(input) {
    let res = this.nilaiTotal(input);

    return res / input.length;
  },
  mid : function(input) {
    let res = 0;

    if (!this.modulus(input.length, 2)) {
      let mid = input.length / 2,
            a = input[mid],
            b = input[mid-1];

        res = (a + b) / 2;
    } else {
      res = input[Math.ceil(input.length / 2)];
    }
    return res;
  },
  total : function(input) {
    let res = 0;

    for (var value of input) {
      res += value;
    }

    return res;
  }
}

module.exports = Calculator;