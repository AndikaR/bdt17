var ex  = module.exports = {};

ex.env = require('../config/env.js');

ex.controller = function(name) {
  return require('../app/user/' + name + '.js');
}

ex.asset = function(path) {
  return '/public/bower_components/' + path;
}